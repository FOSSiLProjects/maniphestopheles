<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - Modify Branch</title>
		<link rel="stylesheet" type="text/css" href="../styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>
	</head>

	<body>

		<header id="header">
			<div class="innertube">
				<a href="../index.php"><img src="../images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
						<h1>Admin &ndash; Modify Branch</h1>
						<hr>

						<?php

						include '../creds.php';

						$branchab = $_POST['branchab'];

						echo "<font color='red'><strong>Modifying branch:</strong> ". $branchab . "</font><br>";

						 ?>

							<form action="" method="post">

									<p>
											<label for="branchfull">Full Branch Name:</label>
											<input type="text" name="branchfull" id="branchfull" autofocus><br><br>

											<label for="branchshort">Branch Abbreviation:</label>
											<input type="text" name="branchshort" id="branchshort"><br><br>

											<label for="branchcode">Branch Code:</label>
											<input type="text" name="branchcode" id="branchcode"><br><br>

											<label for="location">Location</label>
											<input type="text" name="location" id="location"><br><br>

	                </p>

	                <input type="submit" name="submit" value="Submit">
	            </form>

						<BR>
					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="sidebarnav.html"></div>

				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>

<?php

include '../creds.php';


// Check to see if branch exists

$checkbranch = mysqli_query($conn,"SELECT * FROM branches WHERE branchshort = '$branchab'");
$num_rows = mysqli_num_rows($checkbranch);

if ($num_rows != 0) {
	goto ModifyBranch;
}
else {
	header("Location: error-usernotfound.html"); // Branch not found - send to error page
	exit;
}

ModifyBranch:

	$branchfull = $_POST['branchfull'];
	$branchshort = $_POST['branchshort'];
	$branchcode = $_POST['branchcode'];
	$location = $_POST['location'];

mysqli_query($conn,"UPDATE branches SET branchfull = '$branchfull', branchshort = '$branchshort', branchcode = '$branchcode', location = '$location' WHERE branchshort = '$branchab'");

mysql_close($conn);

echo "Success: Branch modified."
?>
