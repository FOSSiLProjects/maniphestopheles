# Maniphestopheles 2.0

## Changelog & Journal

*Note: All dates are DD/MM/YYYY in format.*

All notable changes to this project will be documented in this file.

## Journal

#### Journal Entry 20/03/2018

I removed the user account and user tracking features as there was some worry over whether or not this was a security risk. It's not, but it's just as easy to remove the feature as it is to argue over it. After speaking with some other folks, they agreed that the inclusion of a user name wasn't all that important. The database has been updated to reflect this, though I'm holding the user account table for a few days just in case I run into a bug and need the data to track it down. Some files have been moved to the archive for any bug tracking I might need to do. Eventually that stuff will be deleted.

#### Journal Entry 01/02/2018

More code clean up and link fixes. Things route to index.php, the inbound login page, rather than the non-existent index.html. I've also done a little fiddling with hold routing slips both in look and feel and the method used to generate them. They should look a bit closer to the standard hold routing slips Polaris generates. Along with that, they live in their own directory and can be obtained through new links in sidebarnav.html. That way these slips could be printed after, or at some other time during, the inbound processing.

Finally, I've added another link in sidebarnav.html that take a user to the temp directory where they can get the bin processing list. That way, if something goes sideways, they can obtain that list and feed it to Polaris check in via the Staff Client. Or they can use it to verify items in the bin. Either way, it's available to them.

#### Journal Entry 18/01/2018

I did a bit of code clean up here and there before heading out into the field yesterday to run tests on the system. I'm pleased to report that I ran almost 100 items, real-world circulating items, through inbound and it worked perfectly! As I sorted through the items, I saw a need to allow for the printing of hold-routing slips. I wanted that to be an optional feature so it's a button offered on the post-processing inbound screen. The hold-routing slips open in a browser and can be printed to a receipt printer from there.

#### Journal Entry 11/01/2018

There's now a login form for processing inbound items. This not only provides for the transfer of data across forms, but also builds a structure for getting information from the database, using that to assign variables in PHP, which are then passed along to sipcheckin.py for use. In this case, I've update the branches database to include a given SIP port for the branch, which is used when connecting to the SIP server. The port gets called from the database based on what the user selected when they logged in, and that'll inform the SIP server where the items are being checked in, and it can process them accordingly.

There's a new index that's PHP instead of HTML. That's also the inbound login form. I may change that later but, for now, it'll work. I need to do a bunch of link clean up throughout because of this, but I'll hold off until I hammer down on some ideas for site structure.

#### Journal Entry 10/01/2018

After some time spent learning a bit of Python and the SIP2 protocol, I'm happy to announce that Maniphestopheles now processes bins using SIP2! Before, the system generated a list of items that were to be run through the Polaris check in workform in the Staff Client. While that functionality remains in the code, I've commented it out and updated inboundresult.php to use a Python based system for checking in and processing items. Initial testing looks good, but there are some plumbing needs I'll have to attend to in order to make this work well. More to come soon!


#### Journal Entry 19/05/2017

Aside from bug testing, I'm calling it today -- **Maniphestopheles 2.0 is done!**

I've added user and branch functionality along with the ability to manage users and branches within the browser. Along with that, I've cleaned up some of the code and updated the titles on Admin pages so browser tabs are populated with what's going on with the display. The inbound page has a reset button to clear the form after processing. Finally, the system remembers who you are when processing outbound.

#### Journal Entry 18/05/2017

Modifying and removing users is now a thing, along with a central page for administrative tasks. I should finish similar code for modifying and removing branches today or tomorrow.

#### Journal Entry 17/05/2017

Added the functionality for creating a new user. Error handling is in place so that if a user is created with a username in the database, an error page will be thrown. As always, trying to keep errors and successes as friendly and as useful as possible.

**ADDENDUM @ 14:26**

Got a lot done today, more than I figured. I've added forms to create users and branches and I've updated existing proceedures, mostly outbound, to call data from the database. Dropdown lists are now populated from the database. When processing outbound, a user is first asked for their username and branch (from dropdowns). These get stored as $\_SESSION data and are retained until the user closes the browser.

#### Journal Entry 16/05/2017

This is the Changelog and Journal for Maniphestopheles 2.0, forked from the original code today. To keep the new features completely separate from the running code, I wanted to created a new repository. With that, I'll start this changelog and journal anew with the road to 2.0.

Throughout today I created a new database for users and began working on the basic functionality needed to incorporate a user base into the code. Other parts of the database will require modification, but I'll handle that when it's time.


## Changelog

[TO DO]

* Universal login and user tracking **WON'T DO**
* Better and more error handling
* Database handling of the management of holdslips
* Database handling of the management of bin logs
* List the bin number on the post-processing screen **COMPLETE**
* Link cleanup site wide **COMPLETE, I THINK**

[2.1.5] - **Let's dump user accounts.**

**Removed:** User account features
**Modified:** Admin features re: above and also clean up

[2.1.4] - **Let's label things to make them clearer.**

**Added:** Bin number on the post-processing screen
**Modified:** Changed the file name format for hold slips to make it easier to find them by branch abbreviation.
**Modified:** Removed the version 1.x stuff from this CHANGELOG. See the archive folder for older stuff.

[2.1.3] - **Let's clean things up and provide access to things we've done.**

**Fixed:** index.html links corrected to index.php
**Modified:** Site wide link and code cleanup
**Added:** Link to temp directory so users can get recently processed bins.
**Added:** Link to temp/holdslips directory so users can get recently generated hold slips.

[2.1.2] - **Let's offer hold routing slips.**

**Added:** Updated the SIP2 checkin module to track items trapped as hold routes and then offer a way to print hold slips for shipping.
**Fixed:** index.php title is correct.


[2.1.1] - **Let's make a better workflow.**

**Added:** New index, inbound, and inboundresult pages to allow users to select their branches, which is passed on to the SIP2 checkin module.
**Added:** Updated branches database to include port numbers *Note: Will do a database dump soon.*
**Added:** Inbound processing pulls branch code and port information from the database
**Added:** sipcheckin.py gets branch and port info from inboundresult.php
**Modified:** Bunch of code clean up in sipcheckin.py, along with moving things around to make more sense
**Modified:** Updated both sidebarnav.html files to use the new index.php linkage


[2.1.0] - **Let's use SIP2 instead.**

**Added:** A check in module written in Python that leverages SIP2 processing
**Modified:** inboundresult.php to use the SIP2 check in module

[2.0.0] - **Let's make something even better than before.**

**Added:** Reset button to clear inbound form
**Added:** Session data to remember the user and branch while processing outbound
**Added:** Tweak to outbound workflow to make the above feature possible
**Modified:** All admin pages now have meaningful titles

  ​
