#!/usr/bin/env python3

import time
import sys
import telnetlib
import shutil

# Set up transaction date and time for checkin message
trans_date = str(time.strftime('%Y%m%d'))
trans_time = str(time.strftime('%H%M%S'))

# Checksum calculation thanks to Eli Fulkerson
# https://www.elifulkerson.com/projects/librarystuff.php
def appendChecksum(text):
#calculate checksum
  check = 0
  for each in text:
    check = check + ord(each)
  check = check + ord('\0')
  check = (check ^ 0xFFFF) + 1

  checksum = "%4.4X" % (check)

  text = text + checksum

  return text

# Variables picked up from inboundresult.php
binNumber = sys.argv[1] # For processing the bin file
port = sys.argv[2] # The port number for a given branch
SIPbranch = sys.argv[3] # The branch code used by Polaris
branchshort = sys.argv[4] # The branch abbreviation


# Set up and generate login message

# SIP login credentials - change as needed
SIPuser = " " # Enter your SIP username here
SIPpass = " " # Enter your SIP password here

loginmsg = "9300CN" + SIPuser +"|CO" + SIPpass + "|CP" + SIPbranch + "|AY0AZ"
# Add checksum to login message
login = appendChecksum(loginmsg)


# Server info
sipserver = " " # Change as needed, can be servername or IP


# Set the file to process the items in the bin
binFile = "temp/" + binNumber + ".txt"


# Set up file to handle hold route slips
timestamp = time.strftime("%Y%m%d-%H%M%S")
hrfile = branchshort + "-holdslip-" + binNumber + "-" + timestamp + ".html"
shutil.copy2('temp/holdslips/holdslip.html', 'temp/holdslips/' + hrfile)
holdfile = "temp/holdslips/" + hrfile
h = open(holdfile,"a+")


# Log in to the SIP server
tn = telnetlib.Telnet(sipserver,port) # Change as needed
tn.write((login + "\r\n").encode('ascii'))
tn.read_until(b"941AY0AZFDFD") # Successful login - will add error handling later

# Open the bin file and begin processing the items containted therein
with open(binFile) as file:
    for line in file:
        line = line.strip() # Preprocess line
	# This is the 09 SIP checkin message
        msg = ("09N" + trans_date + "    " + trans_time + trans_date + "    " + trans_time + "AP|AO|AB" + line + "|AC|AY1AZ")
        testmsg = appendChecksum(msg)
        tn.write((testmsg + "\r\n").encode('ascii'))
        time.sleep(1) # Give it some time to process
        GetRetMsg = tn.read_very_eager() # Read the 10 response back from the SIP server
        RetMsg = str(GetRetMsg) # Parse the 10 response for further processing
	# Get the CV code
        CheckString = RetMsg[RetMsg.find("|CV")-1:RetMsg.find("|CY")]
        CVString = (CheckString[4:])
	# Get the item barcode
        BarcodeGet = RetMsg[RetMsg.find("|AB")-1:RetMsg.find("|AQ")]
        Barcode = (BarcodeGet[4:])
	# Get the item title
        TitleGet = RetMsg[RetMsg.find("|AJ")-1:RetMsg.find("|AA")]
        Title = (TitleGet[4:])
	# Get patron name
        PatronGet = RetMsg[RetMsg.find("|DA")-1:RetMsg.find("|AR")]
        Patron = (PatronGet[4:])
	# Get branch info
        BranchGet = RetMsg[RetMsg.find("|CT")-1:RetMsg.find("|CV")]
        Branch = (BranchGet[4:])

	# Process the CV code
        # Item fills local hold
        if CVString == "01":
            print("<font color=\"red\">[LOCAL HOLD for " + Patron + "]</font> " + Barcode + ": " + Title + "<br>")
        # Item is hold-routing
        elif CVString == "02":
            print("<font color=\"red\">[HOLD ROUTING for " + Patron + " @ " + Branch + "]</font> " + Barcode + ": " + Title + "<br>")
            h.write("<div id=\"holdrouteslip\"><h2>" + Branch + "</h2><h1>" + Patron + "</h1><p><br>" + Barcode + "  " + Title + "</p></div><p style=\"page-break-before: always\">")
	# Item is in-transit
        elif CVString == "04":
            print("<font color=\"red\">[IN TRANSIT to " + Branch + "]</font> " + Barcode + ": " + Title + "<br>")
        else:
            print("<font color=\"green\">[CHECKED IN]</font> " + Barcode + " " + Title + "<br>")

tn.close() # Close the connection to the SIP server
h.close() # Close the hold route file

# Indicate the bin that was completed.
print("<h4>Bin " + binNumber + ": COMPLETE</h4><br>")
# Offer the option to process more bins
print("<br><hr><br><input type=\"button\" onclick=\"location.href='inboundcont.php';\" value=\"Process Another Bin\" /> <input type=\"button\" onclick=\"location.href='index.php';\" value=\"Start Over\" />")
# Offer the option to print hold routing slips
print("<br><br><input type=\"button\" onclick=\"location.href='" + holdfile + "';\" value=\"Print Hold Routing Slips\" />")
