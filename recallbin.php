<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - Recall Bin</title>
		<link rel="stylesheet" type="text/css" href="styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>

		<style type="text/css">
			.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
			.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
			.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
			.tg .tg-yw4l{vertical-align:top}
		</style>

	</head>

	<body>

		<header id="header">
			<div class="innertube">
				<a href="index.php"><img src="images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
            <h1>Recall Bin</h1>

            <form action="" method="post">

                <p>
                    <label for="item">Bin Number:</label>
                    <input type="text" name="bin" id="bin" autofocus>
                </p>

                <input type="submit" value="Submit" name="submit">
            </form>

						<br>

						<p><strong>Note:</strong> If you select a list to download, this screen will automatically refresh.</p>
						<table class="tg">
							<tr>
						    <th><strong>Branch</strong></th>
						    <th><strong>Bin</strong></th>
								<th><strong>Items</strong></th>
						    <th><strong>Out Time</strong></th>
								<th><strong>In Time</strong></th>
						  </tr>

						<?php

						include 'creds.php';

						$bin = $_POST['bin'];

						// Run query and loop through the column to write the file
						// Check to see if the form was submitted

						if(isset($_POST['submit'])){


						$result = mysqli_query($conn,"SELECT recordID, bin, branch, itemlist, proctime, intime FROM transaction WHERE bin LIKE '$bin' ORDER BY proctime");

						while($row = mysqli_fetch_array($result))
						{
							$row_recordID = $row['recordID'];
							$row_bin = $row['bin'];
							$row_branch = $row['branch'];
							$row_itemlist = $row['itemlist'];
							$row_time = $row['proctime'];
							$row_intime = $row['intime'];

							echo "<tr>";
							echo "<td>";
							echo $row_branch;
							echo "</td>";
							echo "<td>";
							echo $row_bin;
							echo "</td>";
							echo "<td>";
							echo '<a href="getlist.php?recordID='.$row_recordID.'&bin='.$row_bin.'">Get List</a><br>';
							echo nl2br($row_itemlist);
							echo "</td>";
							echo "<td>";
							echo $row_time;
							echo "</td>";
							echo "<td>";
							echo $row_intime;
							echo "</td>";
							echo "</tr>";
						}
					}

						// Close database connection.
						mysqli_close($conn);
						?>
					</table>

					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="sidebarnav.html"></div>

						<script>
							w3IncludeHTML();
						</script>


				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>
