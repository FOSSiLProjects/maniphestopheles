<?php
session_start();

include 'creds.php';

$bin = $_POST['bin'];
$file = "temp/$bin.txt";
$gb = $_SESSION["branchname"];
$getBranch = mysqli_query($conn,"SELECT branchcode FROM branches WHERE branchshort = '$gb'");
$getBranchResult = mysqli_fetch_assoc($getBranch);
$getPort = mysqli_query($conn,"SELECT port FROM branches WHERE branchshort = '$gb'");
$getPortResult = mysqli_fetch_assoc($getPort);
$Port = $getPortResult['port'];
$BranchNum = $getBranchResult['branchcode'];


// Check to see if bin is found


$checkbin = mysqli_query($conn,"SELECT * FROM outbound WHERE bin = '$bin'");
$num_rows = mysqli_num_rows($checkbin);

if ($num_rows > 0) {
  goto Processing;
}
else {
  header("Location: error-binnotfound.html"); // Bin not found - send to error page
  exit;
}

Processing:
// Archive the transaction

mysqli_query($conn,"INSERT INTO transaction (transID, branch, bin, itemlist, proctime, totalitems) SELECT transID, branch, bin, itemlist, proctime, totalitems FROM outbound WHERE bin = '$bin'");

// Run query and loop through the column to write the file

$result = mysqli_query($conn,"SELECT * FROM outbound WHERE bin = '$bin'");

while($row = mysqli_fetch_array($result))
{
file_put_contents($file, $row['itemlist']);
}

// SIP checkin method
echo '<h1>Bin Processing Complete</h1>';
echo '<h2>Check for exceptions</h2><hr>';

$output = passthru("python3 sipcheckin.py $bin $Port $BranchNum $gb");
echo $output;



/*
// Offer file for download.
// CURRENTLY COMMENTED OUT - PREFERENCE TO SIP CHECKIN METHOD

header("Content-Disposition: attachment; filename=$bin.txt");
header("Content-Type: application/octet-stream");
header("Content-Length: ".filesize("temp/$bin.txt"));
header("Pragma: no-cache");
header("Expires: 0");

$fp = fopen("temp/$bin.txt", "r");

print fread($fp, filesize("temp/$bin.txt"));

fclose($fp);

*/

// Remove the old data from outbound
// CURRENTLY COMMENTED OUT FOR TESTING PURPOSES

// mysqli_query($conn,"DELETE FROM outbound WHERE bin ='$bin'");


// Close database connection.
mysqli_close($conn);

exit();

?>
