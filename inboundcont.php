<?php
session_start();
?>

<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - Inbound</title>
		<link rel="stylesheet" type="text/css" href="styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>
	</head>

	<body>

		<header id="header">
			<div class="innertube">
				<a href="index.php"><img src="images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
            <h1>Inbound</h1>

						<?php
            echo "<strong>Branch:</strong> " . $_SESSION["branchname"] . "<br>";
            ?>

            <form action="inboundresult.php" method="post">



                <p>
                    <label for="bin">Bin Number:</label>
                    <input type="text" name="bin" id="bin" autofocus>
                </p>

                <input type="submit" value="Submit" onclick="return confirm('Process this bin as inbound?')">
								<input type="reset" value="Reset">
            </form>

					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="sidebarnav.html"></div>

						<script>
							w3IncludeHTML();
						</script>

				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>
