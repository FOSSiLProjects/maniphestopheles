-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2018 at 12:24 PM
-- Server version: 10.1.30-MariaDB-0ubuntu0.17.10.1
-- PHP Version: 7.1.15-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maniphestopheles`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `recordID` int(11) NOT NULL COMMENT 'Primary record tracking',
  `branchfull` tinytext NOT NULL COMMENT 'Full name of branch',
  `branchshort` tinytext NOT NULL COMMENT 'Branch abbreviation',
  `branchcode` smallint(6) NOT NULL COMMENT 'Numberical branch code in the ILS',
  `port` smallint(6) NOT NULL COMMENT 'Branch SIP port',
  `location` tinytext NOT NULL COMMENT 'Branch location lat/long'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `outbound`
--

CREATE TABLE `outbound` (
  `transID` int(11) NOT NULL COMMENT 'Primary record tracking',
  `branch` text NOT NULL COMMENT 'Branch abbreviation',
  `bin` int(11) NOT NULL COMMENT 'Bin number',
  `itemlist` text NOT NULL COMMENT 'Item list for the bin',
  `proctime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Time stamp',
  `totalitems` int(11) NOT NULL COMMENT 'Total number of items in bin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `recordID` int(11) NOT NULL COMMENT 'Primary record tracking',
  `transID` int(11) NOT NULL COMMENT 'Record ID from outbound',
  `branch` text NOT NULL COMMENT 'Branch abbreviation',
  `bin` int(11) NOT NULL COMMENT 'Bin number',
  `itemlist` text NOT NULL COMMENT 'List of items in the bin',
  `proctime` datetime NOT NULL COMMENT 'Time processed for outbound',
  `intime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Time processed for inbound',
  `totalitems` int(11) NOT NULL COMMENT 'Total items in the bin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Transactional log for history, stats, tracking, and reports';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`recordID`);

--
-- Indexes for table `outbound`
--
ALTER TABLE `outbound`
  ADD PRIMARY KEY (`transID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`recordID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `recordID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary record tracking';
--
-- AUTO_INCREMENT for table `outbound`
--
ALTER TABLE `outbound`
  MODIFY `transID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary record tracking';
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `recordID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary record tracking';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
