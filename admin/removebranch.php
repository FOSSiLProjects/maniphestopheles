<?php

include '../creds.php';

$branchab = $_POST['branchab'];


// Check to see if branch exists

$checkbranch = mysqli_query($conn,"SELECT * FROM branches WHERE branchshort = '$branchab'");
$num_rows = mysqli_num_rows($checkbranch);

if ($num_rows != 0) {
  goto RemoveBranch;
}
else {
  header("Location: error-branchnotfound.html"); // Branch not found - send to error page
  exit;
}

RemoveBranch:

mysqli_query($conn,"DELETE FROM branches where branchshort = '$branchab'");
mysqli_close($conn);

header("Location: success-branchremoved.html");

?>
