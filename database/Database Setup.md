# Database Setup

If you want to use the default settings for the Maniphestopheles database, this document will walk you through a basic set up. Maniphestopheles is based upon the [MariaDB](https://mariadb.org/) database system and it's highly recommended that you install [phpMyAdmin](https://www.phpmyadmin.net/) for your distro as well. It makes importing and setting up the database much easier.

### Default setup

In phpMyAdmin, create a database user called maniphestopheles and select the option to create a database with the same name. Set the password as you like it, making sure you update creds.php in the root of the site's directory to reflect any changes you make to that password. The default selections will work for the maniphestopheles user, however you want to make sure you give them FILE access. You'll find this under the Data options near the bottom of the create user screen. 

Click the Go button to create the user and the database.

Once the database is created, click on its name in the left sidebar. Select the Import tab at the top of the screen and import the maniphestopheles.sql file. This will set up the structure of the database for your system.