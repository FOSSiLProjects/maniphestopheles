<?php

include '../creds.php';

$branchfull = $_POST['branchfull'];
$branchshort = $_POST['branchshort'];
$branchcode = $_POST['branchcode'];
$location = $_POST['location'];


// Check to see if branch exists

$checkbranch = mysqli_query($conn,"SELECT * FROM branches WHERE branchshort = '$branchshort'");
$num_rows = mysqli_num_rows($checkbranch);

if ($num_rows > 0) {
  header("Location: error-branchexists.html"); // Branch exists - send to error page
  exit;
}

mysqli_query($conn,"INSERT INTO branches (recordID, branchfull, branchshort, branchcode, location) VALUES (NULL,'$branchfull','$branchshort','$branchcode','$location')");
mysqli_close($conn);

header("Location: success-branchadded.html");

?>
