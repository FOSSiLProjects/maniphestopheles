<?php
session_start();

include '../creds.php';

// $branch = $_POST['branch'];
$bin = $_POST['bin'];
$itemlist = $_POST['itemlist'];


// Check to see if bin is aleady in use

$checkbin = mysqli_query($conn,"SELECT * FROM outbound WHERE bin = '$bin'");
$num_rows = mysqli_num_rows($checkbin);

if ($num_rows > 0) {
  header("Location: ../error-bininuse.html"); // Bin in use - send to error page
  exit;
}

// Gather a count of total items entered

$text = $_POST['itemlist'];
$text = strip_tags($text);
// $text .= "\n";
$check = explode("\n", $text);
$lines = count($check);
settype($lines, "integer");
$itemscount = ($lines-1); // Total items entered

mysqli_query($conn,"INSERT INTO outbound (transID,branch,bin,itemlist,totalitems) VALUES (NULL,'{$_SESSION['branchname']}','$bin','$itemlist','$itemscount')");
mysqli_close($conn);

header("Location: ../success-itemsadded.php");

?>
