<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - Remove Branch</title>
		<link rel="stylesheet" type="text/css" href="../styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>
	</head>

	<body>
		<?php include 'access.php'; ?>


		<header id="header">
			<div class="innertube">
				<a href="../index.php"><img src="../images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
						<h1>Admin &ndash; Remove Branch</h1>
						<hr>

						<?php
						// Check for password validity
						if (isset($_POST["password"]) && ($_POST["password"]=="$password")) {
						?>
						<!-- BEGIN Password protected HTML -->

							<form action="removebranch.php" method="post">

									<p>
											<label for="branchab">Branch Abbreviation:</label>
											<input type="text" name="branchab" id="branchab" autofocus><br><br>


	                </p>

	                <input type="submit" value="Submit">
	            </form>

						<!-- END Password protected HTML -->
						<?php
						}
						else
						{
						// Wrong password or no password entered display this message
						if (isset($_POST['password']) || $password == "") {
						  print "<p align=\"center\"><font color=\"red\"><b>Incorrect Password</b><br>Try again.</font></p>";}
						  print "<form method=\"post\"><p align=\"center\">Administrative password<br>";
						  print "<input name=\"password\" type=\"password\" size=\"25\" maxlength=\"10\" autofocus><input value=\"Login\" type=\"submit\"></p></form>";
						}
						?>

						<BR>
					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="adminnav.html"></div>

					<script>
						w3IncludeHTML();
					</script>

				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>
