# Maniphestopheles

## An attempt at tote manifesting for Polaris ILS

### About

Maniphesopheles is a simple tote manifesting system for use with [Polaris ILS](https://www.iii.com/products/polaris-ils/), though it could be targeted toward other ILS solutions and products. As an automated materials handling (AMH) solution, it aims to optimize the processing involved with shipping and receiving materials through a library's multi-branch courier system. In short, rather than dealing items on the individual level, Maniphestopheles deals with bins full of items. Its goals:

* Speed up the processing of items shipped through the courier by focusing on the bins, not the items
* Provide a fast way to processing entire bins of library materials by using the manifest to run the contents through Polaris check-in
* Offer a new way to track items in courier, by providing up to date information on the location of an item at the branch, the bin that item is in, and the time that item was processed into that bin
* Offer a new way to historically track and locate items over long periods of time
* Offer a way to ease the task of finding a misplaced or mis-shipped item by tracking where it was last seen

In its initial proof-of-concept stage, Maniphestopheles was written in [AutoHotkey](https://autohotkey.com/) with a file based storage system for retaining data. After verifying the proof-of-concept, I'm rewriting the app in PHP with a MariaDB backend database for data retention, lookup, and statistical use.

Any questions, feel free to contact [Daniel Messer](mailto:cyberpunklibrarian@protonmail.com).

---

### Screenshots

![Inbound Branch Selection](http://cyberpunklibrarian.com/wp-content/uploads/2018/05/manishot01.png)

![Inbound Bin Check In](http://cyberpunklibrarian.com/wp-content/uploads/2018/05/manishot02.png)

![Outbound](http://cyberpunklibrarian.com/wp-content/uploads/2018/05/manishot03.png)



