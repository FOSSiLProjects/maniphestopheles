<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - Outbound</title>
		<link rel="stylesheet" type="text/css" href="../styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>
	</head>

	<body>

		<header id="header">
			<div class="innertube">
				<a href="../index.php"><img src="../images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
            <h1>Outbound &ndash; Select Branch</h1>

						<form id="userset" action="fillbin.php" method="post">

						<?php
							include '../creds.php';

							echo "<label for='branch'>Branch: </label>";

							$branch = mysqli_query($conn, "SELECT branchshort FROM branches ORDER BY branchshort ASC");

							echo "<select name='branch'>";
							while ($row = mysqli_fetch_array($branch)) {
								echo "<option value='" . $row['branchshort'] . "'>" . $row['branchshort'] . "</option>";
							}
							echo "</select>";
							echo "<br><br>";

							?>

							<input type="submit" value="Submit">

					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="sidebarnav.html"></div>

						<script>
							w3IncludeHTML();
						</script>

				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>
