<!DOCTYPE html>
<!-- Special thanks to quackit.com for this HTML template -->
<!-- Sure I can write this myself, but why reinvent the wheel? -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Maniphestopheles - View Branches</title>
		<link rel="stylesheet" type="text/css" href="../styles/stylesheet.css">
		<script src="http://www.w3schools.com/lib/w3data.js"></script>

	</head>

	<body>

		<header id="header">
			<div class="innertube">
				<a href="index.php"><img src="../images/manny-header.png" style="float:left; padding: 5px 15px 5px 5px;" height="70" border="0"></a>
				<h1>Maniphestopheles</h1>
				<h3>Simple tote manifesting for Polaris ILS</h3>
			</div>
		</header>

		<div id="wrapper">

			<main>
				<div id="content">
					<div class="innertube">
            <h1>View Branches</h1>

						<table class="tg">
							<tr>
						    <th><strong>Branch Full Name</strong></th>
						    <th><strong>Branch Abbreviation</strong></th>
								<th><strong>Branch Code</strong></th>
						    <th><strong>Location</strong></th>
						  </tr>

						<?php

						include '../creds.php';

						$result = mysqli_query($conn,"SELECT branchfull, branchshort, branchcode, location FROM branches WHERE branchshort LIKE '$branchab%' ORDER BY branchfull");

						while($row = mysqli_fetch_array($result))
						{
							$row_branchfull = $row['branchfull'];
							$row_branchshort = $row['branchshort'];
							$row_branchcode = $row['branchcode'];
							$row_location = $row['location'];

							echo "<tr>";
							echo "<td>";
							echo $row_branchfull;
							echo "</td>";
							echo "<td>";
							echo $row_branchshort;
							echo "</td>";
							echo "<td>";
							echo $row_branchcode;
							echo "</td>";
							echo "<td>";
							echo $row_location;
							echo "</td>";
							echo "</tr>";
						}


						// Close database connection.
						mysqli_close($conn);
						?>
					</table>

					</div>
				</div>
			</main>

			<nav id="nav">
				<div class="innertube">
					<div w3-include-html="adminnav.html"></div>

						<script>
							w3IncludeHTML();
						</script>


				</div>
			</nav>

		</div>

		<footer id="footer">
			<div class="innertube">
				<p>Maniphestopheles: Manifesting without complications</p>
			</div>
		</footer>

	</body>
</html>
